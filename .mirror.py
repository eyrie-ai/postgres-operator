import yaml
from subprocess import run as prun, PIPE


CRUNCHY_BASE = 'registry.developers.crunchydata.com/crunchydata/crunchy-'
NEW_BASE = 'registry.gitlab.com/eyrie-ai/postgres-operator/'


def run(cmd_list):
    p = prun(cmd_list)
    p.check_returncode()
    return p 


def mirror(crunchy_base, new_base):
    run(['docker', 'pull', crunchy_base])
    run(['docker', 'tag', crunchy_base, new_base])
    run(['docker', 'push', new_base])


pgo_manager_path = 'config/manager/manager.yaml'

with open(pgo_manager_path, 'r') as f:
    pgo_manager = yaml.safe_load(f)

pgo_manager_env = pgo_manager['spec']['template']['spec']['containers'][0]['env']

for e in pgo_manager_env:
    if e.get('value', '').find(CRUNCHY_BASE) == 0:
        crunchy_base = e['value']
        new_base = crunchy_base.replace(CRUNCHY_BASE, NEW_BASE)
        mirror(crunchy_base, new_base)

pgo_kustomization_path = 'config/default/kustomization.yaml'

with open(pgo_kustomization_path, 'r') as f:
    pgo_kustomization = yaml.safe_load(f)

for i in pgo_kustomization['images']:
    if i.get('name', '') == 'postgres-operator':
        crunchy_base = i['newName'] + ':' + i['newTag']
        new_base = NEW_BASE + 'postgres-operator' + ':' + i['newTag']
        mirror(crunchy_base, new_base)


